﻿using UnityEngine;
using System.Collections;

public class GizmoPos : MonoBehaviour {

    public float GizmoSize = 0.75f;
    public Color GizmoColor = Color.yellow;
	
	// Update is called once per frame
	void OnDrawGizmos () {
        Gizmos.color = GizmoColor;
        Gizmos.DrawWireSphere(transform.position, GizmoSize);
	
	}
}
