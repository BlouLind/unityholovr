﻿using UnityEngine;
using System.Collections;

public class LookAtPlayer : MonoBehaviour {
    private GameObject PlayerPos;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        PlayerPos = GameObject.FindGameObjectWithTag("Player");
        gameObject.transform.LookAt(PlayerPos.transform);
	
	}
}
