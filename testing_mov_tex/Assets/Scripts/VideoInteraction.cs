﻿using UnityEngine;
using System;
using System.Collections;

public class VideoInteraction : MonoBehaviour {
    //interaction variables
    private Vector3 myposition;
    private Vector3 playerposition;
    private Vector2 anglevector;
    private float angle;
    private Vector2[] anglesegment;
    private float anglechoice;
    private float[] angleselect;
    private int posid;

    private GameObject room;
    public float disfrommiddle = -3.0f;


    //video variables 
    public float possibleviewangle = 90;
    public int numberovideo = 25;
    public Vector2 resolution = new Vector2(1920,1080);
    public Vector2 xyvideos = new Vector2(5, 5);
    private float xmove;
    private float ymove;

    void Start () {

        
        // get how much the video should move
        xmove =  (resolution.x/10)/xyvideos.x;
        ymove =  (resolution.y/10)/xyvideos.y;

        GameObject.FindGameObjectWithTag("refpos").transform.position = new Vector3(xmove/2,-ymove+2,disfrommiddle);


        //get the value by which we need to scale the video plane
        float xscale = resolution.x / 100;
        float yscale = resolution.y / 100;
        // saves a reference for the room we need to be in 
        room = GameObject.FindGameObjectWithTag("ConcertRoom");
        // scale the videoplane
        transform.localScale = new Vector3(xscale, 1, yscale);
        // scale and moves the room to the location to be shown
        room.transform.localScale = new Vector3(xmove/2, xmove/2, xmove/2);
       // room.transform.position = new Vector3(xmove/2,-ymove/2,-xmove);
        // move the video plane to a start position
        transform.position = new Vector3((resolution.x / 2)/10, (-resolution.y / 2) /10, disfrommiddle);
        // move the player to a position in the room 
        GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(room.transform.position.x+xmove, room.transform.position.y-ymove, room.transform.position.z-xmove);
        // creates the array that will hold the different position that we need to move the videoplane to
        anglesegment = new Vector2[(int)xyvideos.x * (int)xyvideos.y];
        // fill in the data
        for(int i = 0; i<5; i++)
        {
            for(int j = 0; j < 5; j++)
            {
                anglesegment[(i*(int)xyvideos.x)+j] = new Vector2((-xmove * (5-j)+ xmove + (resolution.x / 2) / 10), ymove * i+(-resolution.y / 2) / 10);
            }
        }
        anglechoice = possibleviewangle /numberovideo;
        angleselect = new float[numberovideo];
        for(int i = 1; i<= numberovideo ; i++)
        {
            angleselect[i - 1] = anglechoice * i + (possibleviewangle / 2);
        }
        foreach(Vector2 i in anglesegment)
        {
            Debug.Log(i);
        }
       
    }
	
	// Update is called once per frame
	void Update () {
        // getting the position in the middle of the room
        myposition = GameObject.FindGameObjectWithTag("refpos").transform.position;
        // getting the players position
        playerposition = GameObject.FindGameObjectWithTag("Player").transform.position;
        // calculating the vector that we need to find the angle
        anglevector = new Vector2(myposition.x - playerposition.x, myposition.z - playerposition.z);
        //calculating the angle
        angle = 90+(float)(Math.Atan2(anglevector.x, anglevector.y)*(180/Math.PI));

        for (int i = 0; i < anglesegment.Length; i++)
        {
            if (angle > (angleselect[i] - (anglechoice / 2)) && angle < (angleselect[i] + (anglechoice / 2)) && i != posid)
            {
                posid = i;
                GameObject.FindGameObjectWithTag("refpos").transform.rotation = Quaternion.identity;
                gameObject.transform.position = new Vector3(anglesegment[i].x, anglesegment[i].y, disfrommiddle);
                GameObject.FindGameObjectWithTag("refpos").transform.LookAt(playerposition);
            }
        }



        Debug.Log(angle);

        
	} 
}
